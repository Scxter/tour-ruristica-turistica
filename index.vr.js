import React from 'react';
import { AppRegistry } from 'react-vr';
import { LiveTour } from 'live-tour-lab';


class RecorridoGir360 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: 'auditorio.json'
    }
  }

  render() {
    return (
      <LiveTour tourURI={this.state.url} />
    );
  }
};

AppRegistry.registerComponent('RecorridoGir360', () => RecorridoGir360);
